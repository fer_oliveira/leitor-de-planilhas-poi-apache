package exemplo.leitura;

import javax.swing.JOptionPane;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Iniciando a leitura");
		
		ExemploLeitura exemploLeitura = new ExemploLeitura();
		
		String dir = JOptionPane.showInputDialog(null, "Digite o diretório em que se encontra as planilhas.", "Diretório", 1);
		exemploLeitura.setDiretorio(dir);
		try {
			exemploLeitura.teste1();
		} catch (Exception e) {
			throw new IllegalArgumentException("Falha ao ler os dados da planilha", e);
		}
	}
}