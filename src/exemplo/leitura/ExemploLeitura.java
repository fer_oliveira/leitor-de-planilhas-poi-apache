package exemplo.leitura;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream.GetField;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExemploLeitura {
	
	private String diretorio;
	
	protected File[] getFiles() {
		File file = new File(getDiretorio());
		return file.listFiles();
	}
	
	protected String getCellValue(Cell cell) {
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			long value = (long) cell.getNumericCellValue();
			return Long.toString(value) + " | Tipo - Numerico";
		} else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			return cell.getStringCellValue() + " | Tipo - String";
		}
		return null;
	}
	
	public void teste1() throws Exception {
	
		File[] files = this.getFiles();
		
		for (File file : files) {
			String fileName = file.getName();
			if (fileName.endsWith(".xls") || fileName.endsWith(".xlsx")) {
				
				Workbook workbook = WorkbookFactory.create(file);				
				Sheet sheet = workbook.getSheetAt(0);
				
				int numeroDePlanilhas = workbook.getNumberOfSheets();
				
				System.out.println("Arquivo: " + fileName.toString());
				System.out.println("Número total de planilhas: " + numeroDePlanilhas);
				
				if(numeroDePlanilhas > 1) {
					
					for (int i = 0; i < numeroDePlanilhas; i++) {
						
						 sheet = workbook.getSheetAt(i); 
						 System.out.println("Iniciando a leitura da planilha : " + workbook.getSheetName(i));
						 this.iteraSobreAsCelulasDeCadaLinha(sheet, fileName);
						 System.out.println("TERMINO DA PLANILHA");
						 System.out.println("------------------------------------------------------------");
					}
					
				} else {
					System.out.println("Iniciando a leitura da planilha : " + workbook.getSheetName(0));
					this.iteraSobreAsCelulasDeCadaLinha(sheet, fileName);
					System.out.println("TERMINO DA PLANILHA");
					System.out.println("------------------------------------------------------------");
					
				}
			}
		}
	}
	
	private void iteraSobreAsCelulasDeCadaLinha(Sheet sheet, String fileName) {
		int rows = sheet.getLastRowNum();
		
		for (int i = 0; i <= rows; i++) {
				Row row = sheet.getRow(i);
				for (int j = 0; j < row.getLastCellNum() ; j++){
					System.out.println("Linha: " + (i + 1) + " - Coluna: "+ (j+1) + " - Valor: " + this.getCellValue(row.getCell(j)));
				}
		}
	}

	public String getDiretorio() {
		return diretorio;
	}

	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}
}
